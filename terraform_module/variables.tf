variable "namespace" {
  type = string
}
variable "chart_name" {
  type = string
}
variable "chart_version" {
  type    = string
  default = "0.1.3"
}
variable "values" {
  type    = list(string)
  default = []
}
variable "image_repository" {
  type    = string
  default = "traefik"
}
variable "image_tag" {
  type    = string
  default = "2.8.0"
}
variable "middleware_image_repository" {
  type    = string
  default = "thomseddon/traefik-forward-auth"
}
variable "middleware_image_tag" {
  type    = string
  default = "2"
}

variable "helm_force_update" {
  type    = bool
  default = false
}
variable "helm_recreate_pods" {
  type    = bool
  default = false
}
variable "helm_cleanup_on_fail" {
  type    = bool
  default = false
}
variable "helm_max_history" {
  type    = number
  default = 0
}
variable "helm_timeout" {
  type    = number
  default = 300
}

variable "limits_cpu" {
  type    = string
  default = null
}
variable "limits_memory" {
  type    = string
  default = null
}
variable "requests_cpu" {
  type    = string
  default = null
}
variable "requests_memory" {
  type    = string
  default = null
}
