variable "ingress_host" {
  type    = string
  default = null
}
variable "auth_paths" {
  type    = list(string)
  default = []
}
variable "auth_target_service" {
  type    = string
  default = null
}
